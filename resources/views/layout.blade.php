<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Curso de Lumen</title>
    <link rel="stylesheet" type="text/css" href="assets/js/semantic.min.css">
</head>
<body>

    <div class="container">
        @yield('content')
    </div>
    <script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <script src="assets/js/semantic.min.js"></script>
</body>
</html>