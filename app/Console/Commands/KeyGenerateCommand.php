<?php

namespace App\Console\Commands;

use Illuminate\Console\Command as ConsoleCommand;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class KeyGenerateCommand extends ConsoleCommand
{
    protected $name = "key:generate";

    protected $description = "Criar a chave da aplicação";

    public function handle()
    {
        $this->fire();
    }

    public function fire() 
    {
        $key = $this->getRandomKey();

        if ($this->option('show')) {
            return $this->line("<comment> {$key} </comment>");
        }

        $path = base_path('.env');

        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                $this->laravel['config']['app.key'], $key, file_get_contents($path)
            ));
        }

        $this->laravel['config']['app.key'] =  $key;
        $this->info("Application key [$key] set successfully.");
    }

    public function getRandomKey() 
    {
        return Str::random(32);
    }

    public function getOptions()
    {
        return [
            ['show', null, InputOption::VALUE_NONE, 'Simply display the key instead of modifying  files.'],
        ];
    }   
}