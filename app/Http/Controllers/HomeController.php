<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function index()
    {
        $data = [
            'nome' => 'Vinicius'
        ];
        return view('home')->with($data);
    }
}